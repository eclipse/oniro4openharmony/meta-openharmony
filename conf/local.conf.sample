# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

#
# This file is your local configuration file and is where all local user settings
# are placed. The comments in this file give some guide to the options a new user
# to the system might want to change but pretty much any configuration option can
# be set in this file. More adventurous users can look at local.conf.extended
# which contains other examples of configuration which can be placed in this file
# but new users likely won't need any of them initially.
#
# Lines starting with the '#' character are commented out and in some cases the
# default values are provided as comments to show people example syntax. Enabling
# the option is a question of removing the # character and making any change to the
# variable as required.


# Machine Selection
#
# You need to select a specific machine to target the build with. There are a selection
# of emulated machines available which can boot and run in the QEMU emulator:
#
#MACHINE ?= "qemuarma7"
#
# This sets the default machine to qemuarma7 if no other machine is selected:
MACHINE ??= "qemuarma7"

#
# SDK target architecture
#
# This variable specifies the architecture to build SDK items for and means
# you can build the SDK packages for architectures other than the machine you are
# running the build on (i.e. building i686 packages on an x86_64 host).
# Supported values are i686 and x86_64
#SDKMACHINE ?= "i686"

#
# Default policy config
#
DISTRO ?= "oniro-openharmony-linux"
# Comment this out if you don't want to build ACTS test suite
DISTRO_FEATURES:append = " acts"

#
# Extra image configuration defaults
#
# The EXTRA_IMAGE_FEATURES variable allows extra packages to be added to the generated
# images. Some of these options are added to certain image types automatically. The
# variable can contain the following options:
#  "dbg-pkgs"       - add -dbg packages for all installed packages
#                     (adds symbol information for debugging/profiling)
#  "src-pkgs"       - add -src packages for all installed packages
#                     (adds source code for debugging)
#  "dev-pkgs"       - add -dev packages for all installed packages
#                     (useful if you want to develop against libs in the image)
#  "ptest-pkgs"     - add -ptest packages for all ptest-enabled packages
#                     (useful if you want to run the package test suites)
#  "tools-sdk"      - add development tools (gcc, make, pkgconfig etc.)
#  "tools-debug"    - add debugging tools (gdb, strace)
#  "eclipse-debug"  - add Eclipse remote debugging support
#  "tools-profile"  - add profiling tools (oprofile, lttng, valgrind)
#  "tools-testapps" - add useful testing tools (ts_print, aplay, arecord etc.)
#  "debug-tweaks"   - make an image suitable for development
#                     e.g. ssh root access has a blank password
# There are other application targets that can be used here too, see
# meta/classes/image.bbclass and meta/classes/core-image.bbclass for more details.
# We default to enabling the debugging tweaks.
EXTRA_IMAGE_FEATURES ?= "debug-tweaks"

#
# Parallelism Options
#
# These two options control how much parallelism BitBake should use. The first
# option determines how many tasks bitbake should run in parallel:
#
#BB_NUMBER_THREADS ?= "4"
#
# Default to setting automatically based on cpu count
#BB_NUMBER_THREADS ?= "${@oe.utils.cpu_count()}"
#
# The second option controls how many processes make should run in parallel when
# running compile tasks:
#
#PARALLEL_MAKE ?= "-j 4"
#
# Default to setting automatically based on cpu count
#PARALLEL_MAKE ?= "-j ${@oe.utils.cpu_count()}"
#
# For a quad-core machine, BB_NUMBER_THREADS = "4", PARALLEL_MAKE = "-j 4" would
# be appropriate for example.

#
# Removes sources after build to conserve diskspace
#
INHERIT += "rm_work"
RM_WORK_EXCLUDE += "qemu-helper-native"

#
# Use source mirror in China
#
# Uncomment this if you are building in China, and/or have trouble fetching sources.
#
#INHERIT += "own-mirrors"
#SOURCE_MIRROR_URL = "http://114.116.235.68/source-mirror"

#
# Use out-of-China mirror of OpenHarmony git repositories.
#
MIRRORS:prepend = "git://gitee.com/openharmony/ git://github.com/openharmony/ \n "

#
# Choose OpenHarmony version
#
# Uncomment to override the variable OPENHARMONY_VERSION set in the
# distro configuration file.
#
#OPENHARMONY_VERSION = "3.1"

USER_CLASSES += "buildstats buildstats-summary"

# Uncomment if would like to use the Waveshare "C" 1024×600, 7 inch Capacitive 
# Touch Screen LCD
# WAVESHARE_1024X600_C_2_1 = "1"
